local function ProNames(msg)
if msg.content.text then
text = msg.content.text.text
end
--- Start Code ---
if text and (text:match("^وضع توحيد (.*)$") or text:match("^ضع توحيد (.*)$")) then
if Manager(msg) then
if DevRio:get(David.."Rio:Lock:ProNames"..msg.chat_id) then
local Txt = text:match("^وضع توحيد (.*)$") or text:match("^ضع توحيد (.*)$")
LuaTele.sendText(msg.chat_id, msg.id,'↯︙تم تعيين ↫ '..Txt..' كتوحيد للمجموعه')
DevRio:set(David.."Rio:ProNames:Txt"..msg.chat_id,Txt)
else
LuaTele.sendText(msg.chat_id, msg.id,'↯︙ميزة التوحيد معطله يرجى تفعيلها')
end
end
end
if text and (text:match("^حذف توحيد (.*)$") or text:match("^مسح توحيد (.*)$")) then
if Manager(msg) then
if DevRio:get(David.."Rio:Lock:ProNames"..msg.chat_id) then
local Txt = text:match("^حذف توحيد (.*)$") or text:match("^مسح توحيد (.*)$")
LuaTele.sendText(msg.chat_id, msg.id,'↯︙تم حذف توحيد المجموعه')
DevRio:del(David.."Rio:ProNames:Txt"..msg.chat_id)
else
LuaTele.sendText(msg.chat_id, msg.id,'↯︙ميزة التوحيد معطله يرجى تفعيلها')
end
end
end
if text and (text:match("^تعين عدد الكتم (.*)$") or text:match("^تعيين عدد الكتم (.*)$")) then
if Manager(msg) then
if DevRio:get(David.."Rio:Lock:ProNames"..msg.chat_id) then
local Num = text:match("^تعين عدد الكتم (.*)$") or text:match("^تعيين عدد الكتم (.*)$")
LuaTele.sendText(msg.chat_id, msg.id,'↯︙تم تعيين  ↫ '..Num..' عدد الكتم')
DevRio:set(David.."Rio:ProNames:Num"..msg.chat_id,Num)
else
LuaTele.sendText(msg.chat_id, msg.id,'↯︙ميزة التوحيد معطله يرجى تفعيلها')
end
end
end
if DevRio:get(David.."Rio:Lock:ProNames"..msg.chat_id) then
if text == "التوحيد" or text == "توحيد" then
if DevRio:get(David.."Rio:ProNames:Txt"..msg.chat_id) then
local ProNamesTxt = DevRio:get(David.."Rio:ProNames:Txt"..msg.chat_id)
local ProNamesNum = DevRio:get(David.."Rio:ProNames:Num"..msg.chat_id) or 5
LuaTele.sendText(msg.chat_id, msg.id,'↯︙التوحيد هو ↫ '..ProNamesTxt..'\n↯︙عدد المحاولات للكتم ↫ '..ProNamesNum)
else
LuaTele.sendText(msg.chat_id, msg.id,'↯︙لم يتم تعيين توحيد للمجموعه')
end
end
end
if not msg.forward_info and not Constructor(msg) then
if DevRio:get(David.."Rio:Lock:ProNames"..msg.chat_id) and DevRio:get(David.."Rio:ProNames:Txt"..msg.chat_id) then 
local result = LuaTele.getUser(msg.sender_id.user_id)
if result and result.first_name then 
if result.first_name:match("(.*)"..DevRio:get(David.."Rio:ProNames:Txt"..msg.chat_id).."(.*)") then 
DevRio:srem(David..'Rio:Muted:'..msg.chat_id, msg.sender_id.user_id)
else
local ProNamesTxt = DevRio:get(David.."Rio:ProNames:Num"..msg.chat_id) or 5
local UserNum = DevRio:get(David.."Rio:ProNames:UserNum"..msg.chat_id..msg.sender_id.user_id) or 0
if (tonumber(UserNum) == tonumber(ProNamesTxt) or tonumber(UserNum) > tonumber(ProNamesTxt)) then 
DevRio:sadd(David..'Rio:Muted:'..msg.chat_id, msg.sender_id.user_id)
DevRio:del(David.."Rio:ProNames:UserNum"..msg.chat_id..msg.sender_id.user_id)
LuaTele.sendText(msg.chat_id, msg.id,"↯︙العضو ↫ ["..result.first_name.."](https://t.me/"..(result.username or "L9L9L")..")\n↯︙تم كتمه بسبب عدم وضع توحيد المجموعه بجانب اسمه يجب عليه وضع التوحيد وسوف يتم الغاء كتمه تلقائيا",'md')
else 
DevRio:incrby(David.."Rio:ProNames:UserNum"..msg.chat_id..msg.sender_id.user_id,1)
LuaTele.sendText(msg.chat_id, msg.id, "↯︙عذرا عزيزي ↫ ["..result.first_name.."](https://t.me/"..(result.username or "L9L9L")..")\n↯︙عليك وضع التوحيد ↫ `"..DevRio:get(David.."Rio:ProNames:Txt"..msg.chat_id).."` بجانب اسمك\n↯︙عدد المحاولات المتبقيه ↫ "..(tonumber(ProNamesTxt) - tonumber(UserNum)).."",'md')
end
end
end
end
end
if text == "تفعيل التوحيد" and Constructor(msg) then
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم تفعيل توحيد المجموعه')
DevRio:set(David.."Rio:Lock:ProNames"..msg.chat_id,true)
end
if text == "تعطيل التوحيد" and Constructor(msg) then
LuaTele.sendText(msg.chat_id, msg.id, '↯︙تم تعطيل توحيد المجموعه')
DevRio:del(David.."Rio:Lock:ProNames"..msg.chat_id)
end
--- End Function ---
end
return {David = ProNames}
